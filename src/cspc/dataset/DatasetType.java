package cspc.dataset;

public enum DatasetType {
	WDATA("wdata"),
	CSV("csv");

	private final String description;

	private DatasetType(String description) {
		this.description = description;
	}

	public String toString() {
		return this.description;
	}
}
