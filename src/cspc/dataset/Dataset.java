package cspc.dataset;

import cspc.feature.Feature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.BitSet;

public class Dataset {
	private final List<Feature> _list;
	private final Map<Feature, Integer> _map;
	private int _numberOfVariables;
	private List<Integer> _card;


	public Dataset() {
		this._list = new ArrayList<Feature>();
		this._card = new ArrayList<Integer>();
		this._numberOfVariables = 0;
		this._map = new HashMap<Feature, Integer>();
	}

	// only add unique examples
	public void addExample(Feature example) {
		Integer value = this._map.get(example);

		if (value == null) {
			this._map.put(example, 1);
			this.getList().add(example);
		} else {
			this._map.put(example, value + 1);
		}
	}

	public int getCount(Feature example) {
		Integer count = this._map.get(example);
		return (count == null) ? 0 : count;
	}

	public int getCountFromIndex(int index) {
		return this.getCount(this._list.get(index));
	}

	public List<Feature> getUniqueFeatures() {
		java.util.Set<Feature> setOfFeatures = this._map.keySet();
		List<Feature> uniqueFeatures = new ArrayList<Feature>(setOfFeatures.size());

		for (Feature feature : setOfFeatures) uniqueFeatures.add(feature.clone());

		return uniqueFeatures;
	}

	public Feature getExample(int index) {
		return this.getList().get(index).clone();
	}

	public List<Feature> getElements() {
		return Collections.unmodifiableList(this.getList());
	}

	public List<Feature> getList() {
		return this._list;
	}

	public int getNumberOfVariables()
	{
                return this._numberOfVariables;
        }

        public void setNumberOfVariables(int numberOfVariables)
	{
                this._numberOfVariables = numberOfVariables;
        }

	public void setCard(List<Integer> card) {
		this._card = card;
	}

	public int getCard(int var) {
		return this._card.get(var);
	}

	public List<Integer> getCard() {
		return Collections.unmodifiableList(this._card);
	}

	public Map<Feature, Integer> getMap() {
		return _map;
	}

	public int getNumberOfMatchingDataPoints(Feature f) {
		int count = 0;
		for (Feature feature : _list){
			if(f.isSatisfied(feature)){
				count++;
			}
		}
		return count;
	}

	public int[]
	computeSufficientStatistics(BitSet vars, Map<Integer,Integer> context) {
		BitSet valuedVars = new BitSet(this.getNumberOfVariables());
		BitSet nonValuedVars = new BitSet(this.getNumberOfVariables());

		int numberOfCells = 1;
		for(int i = vars.nextSetBit(0); i >= 0; i = vars.nextSetBit(i+1))
			if (context.containsKey(i)) {
				valuedVars.set(i, true);

			} else {
				nonValuedVars.set(i, true);
				numberOfCells *= this.getCard(i);
			}

		int[] counts = new int[numberOfCells];

		int idx, val, k;
		// loop over examples in dataset
		int index = 0;
		for (Feature example : this._list) {
			idx = 0;
			k = 0;

			// check compatibility with context
			boolean flag=true;
			for(int i = valuedVars.nextSetBit(0); i >= 0; i = valuedVars.nextSetBit(i+1)) {
				val = example.getVal(i);

				if (context.containsKey(i) && (context.get(i) != val))
					flag = false;
			}

			if (!flag) continue;

			for(int i = nonValuedVars.nextSetBit(0);
			    i >= 0;
			    i = nonValuedVars.nextSetBit(i+1)) {
				val = example.getVal(i);

				if (val != 0)
					idx += (int)Math.pow(2, nonValuedVars.cardinality() - 1 - k);
				k++;
			}

			counts[idx] += this.getCount(example);
		}

		return counts;
	}

	public int[]
	computeSufficientStatistics(BitSet vars, Map<Integer,Integer> context, BitSet records) {
		BitSet valuedVars = new BitSet(this.getNumberOfVariables());
		BitSet nonValuedVars = new BitSet(this.getNumberOfVariables());

		int numberOfCells = 1;
		for(int i = vars.nextSetBit(0); i >= 0; i = vars.nextSetBit(i+1))
			if (context.containsKey(i)) {
				valuedVars.set(i, true);

			} else {
				nonValuedVars.set(i, true);
				numberOfCells *= this.getCard(i);
			}

		int[] counts = new int[numberOfCells];

		int idx, val, k;
		// loop over examples in dataset
		int index = 0;

		Feature example;
		for(int j = records.nextSetBit(0); j >= 0; j = records.nextSetBit(j+1)) {
			example = this._list.get(j);

			idx = 0;
			k = 0;

			// check compatibility with context
			boolean flag=true;
			for(int i = valuedVars.nextSetBit(0); i >= 0; i = valuedVars.nextSetBit(i+1)) {
				val = example.getVal(i);

				if (context.containsKey(i) && (context.get(i) != val))
					flag = false;
			}

			if (!flag) continue;

			for(int i = nonValuedVars.nextSetBit(0);
			    i >= 0;
			    i = nonValuedVars.nextSetBit(i+1)) {
				val = example.getVal(i);

				if (val != 0)
					idx += (int)Math.pow(2, nonValuedVars.cardinality() - 1 - k);
				k++;
			}

			counts[idx] += this.getCount(example);
		}

		return counts;
	}
}