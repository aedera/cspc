/*
 * VaryNode.java
 * Copyright (C) 2002-2012 University of Waikato, Hamilton, New Zealand
 *
 */

package cspc.adtree;

import cspc.dataset.Dataset;

/**
 * Part of ADTree implementation. See ADNode.java for more details.
 *
 */

public class VaryNode implements java.io.Serializable {
  /** index of the node varied **/
  public int m_iNode;

  /** most common value **/
  public int m_nMCV;

  /** list of ADNode children **/
  public ADNode [] m_ADNodes;

  /** Creates new VaryNode */
  public VaryNode(int iNode) {
    m_iNode = iNode;
  }

  /**
   * print is used for debugging only, called from ADNode
   *
   * @param sTab amount of space.
   */
  public void print(String sTab) {
    for (int iValue = 0; iValue < m_ADNodes.length; iValue++) {
      System.out.print(sTab + iValue + ": ");
      if (m_ADNodes[iValue] == null) {
	if (iValue == m_nMCV) {
	  System.out.println("MCV");
	} else {
	  System.out.println("null");
	}
      } else {
	System.out.println();
	m_ADNodes[iValue].print();
      }
    }
  }
}
