package cspc;

import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.Collection;
import java.util.BitSet;
import java.util.List;
import java.util.Vector;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Collection;

import cspc.feature.*;

import cspc.independenceTest.IndependenceTest;
import cspc.independenceTest.ChiSquare;

import cspc.setting.Configuration;
import cspc.setting.FeatureType;

import cspc.util.Util;
import cspc.util.FileHandler;

import cspc.dataset.Dataset;
import cspc.adtree.ADNode;


public class StructureLearner {
	public static void main(String[] args) {
		long startTime;
		long totalTime;

		if (args.length < 2) {
			System.err.println("Please provide " +
					   "the path to a configuration file, " +
					   "and a dataset name.");
			System.exit(1);
		}

		try {
			Configuration conf = (args.length > 2) ?
				new Configuration(args[0], args[1], args[2]) :
				new Configuration(args[0], args[1]);

			if (!conf.getDatasetFile().exists()) {
				System.err.println("Dataset file does not exist.");
				System.exit(1);
			}

			// Read dataset from file
			Dataset dataset = FileHandler.readDataset(conf.getDatasetFile());

			// Set cardinalities
			Vector<Integer> card = new Vector<Integer>();
			for (int i = 0; i < dataset.getNumberOfVariables(); i++)
			    card.add(2);

			dataset.setCard(card);

			// Generate the set of contexts
			List<Integer> contexts = null;
			if (!conf.getContextsFile().exists()) {
				contexts = new java.util.ArrayList<Integer>();

				for (int i = 0; i < dataset.getList().size(); i++)
					contexts.add(i);
			} else {
				if (!conf.getContextsFile().exists()) {
					System.err.println("file of contexts does not exist.");
					System.exit(1);
				}

				contexts = FileHandler.readContexts(conf.getContextsFile());
			}

			// Make ADTree for caching sufficient
			// statistics used by independence test
			ADNode ADTree = null;
			if (conf.getCacheForIndependenceTests()) {
				ADNode.MIN_RECORD_SIZE = conf.getCacheThreshold();

				// check lower bound
				if (ADNode.MIN_RECORD_SIZE < 0)
					ADNode.MIN_RECORD_SIZE = 0;

				// check upper bound
				if (ADNode.MIN_RECORD_SIZE > dataset.getList().size())
					ADNode.MIN_RECORD_SIZE = dataset.getList().size();

				// make the root node
				ADTree = ADNode.makeADNode(dataset);
			}

			// Create the independence test from configuration file
			IndependenceTest test =
				(conf.getIndependenceTestName().contentEquals("chi")) ?
				new ChiSquare(dataset, conf.getIndependenceTestThreshold()) : //testing!!
				new ChiSquare(dataset, conf.getIndependenceTestThreshold());

			// Set the cache in the independence test
			test.setADTree(ADTree);

			// Initialize cspc
			Algorithm cspc = new Algorithm(dataset.getNumberOfVariables(),
						       test,
						       dataset,
						       conf);

			// Execute cspc to learn contextualized structures
			startTime = System.currentTimeMillis();

			Map<Structure,List<Integer>> structures = cspc.execute(contexts);

			totalTime = System.currentTimeMillis() - startTime;

			FileHandler.writeMessage(conf.getLogFile(),
						 "Generated " + structures.keySet().size() +
						 " structures for " +
						 contexts.size() +
						 " contexts in " + totalTime + " ms.");

			FileHandler.writeMessage(conf.getLogFile(),
						 "Number of tests:" + cspc.getNumberOfTests());

			FileHandler.writeMessage(conf.getLogFile(),
						 "Number of weighted tests:" + cspc.getNumberOfWeightedTests());

			// Clean output file where cspc will write on it its the output
			FileHandler.writeNetwork(null, conf.getNetworkFile());

			// Write output structure
			FileHandler.writeNetwork(structures, conf.getNetworkFile());

			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();

			System.exit(1);
		}
	}
}