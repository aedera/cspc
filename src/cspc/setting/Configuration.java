package cspc.setting;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Vector;
import java.util.Scanner;


public class Configuration {
	private final String _datasetName;
	private final String _datasetDirectory;
	private final String _networkDirectory;
	private final String _logDirectory;
	private final String _independenceTestName;
	private final double _independenceTestThreshold;
	private final FeatureType _featureType;
	private final String _setOfContextsType;
	private final boolean _cacheForIndependenceTests;
	private final int _cacheThreshold;
	private final int _maxTreeWidth;
	private final String _contextsFile;

	public
	Configuration(String pathToConfigurationFile, String datasetName)
		throws FileNotFoundException, IOException {
		this(pathToConfigurationFile, datasetName, "");
	}

	// Read the configuration
	public
	Configuration(String pathToConfigurationFile,
		      String datasetName,
		      String contextsFile)
		throws FileNotFoundException, IOException {
		this._datasetName = datasetName;
		Scanner scanner = new Scanner(new File(pathToConfigurationFile));
		this._datasetDirectory = scanner.nextLine();
		this._networkDirectory = scanner.nextLine();
		this._logDirectory = scanner.nextLine();

		// Set type of feature
		String strFeatureType = scanner.nextLine();

		if (strFeatureType.equalsIgnoreCase("positive"))
			this._featureType = FeatureType.POSITIVE;
		else if (strFeatureType.equalsIgnoreCase("mixed"))
			this._featureType = FeatureType.MIXED;
		else
			this._featureType = FeatureType.BINARY;

		this._setOfContextsType = scanner.nextLine();
		this._independenceTestName = scanner.nextLine();
		this._independenceTestThreshold = Double.parseDouble(scanner.nextLine());
		this._cacheForIndependenceTests = Boolean.parseBoolean(scanner.nextLine());
		this._cacheThreshold = Integer.parseInt(scanner.nextLine());
		this._maxTreeWidth = Integer.parseInt(scanner.nextLine());
		this._contextsFile = contextsFile;
	}

	public Configuration(String datasetName,
			     String datasetDirectory,
			     String networkDirectory,
			     String logDirectory,
			     String independenceTestName,
			     double thresholdIndependenceTest,
			     FeatureType featureType,
			     String setOfContextsType,
			     boolean cacheForIndependenceTests,
			     int cacheThreshold,
			     int maxTreeWidth,
			     String contextsFile) {
		this._datasetName = datasetName;
		this._datasetDirectory = datasetDirectory;
		this._networkDirectory = networkDirectory;
		this._logDirectory = logDirectory;
		this._independenceTestName = independenceTestName;
		this._independenceTestThreshold = thresholdIndependenceTest;
		this._featureType = featureType;
		this._setOfContextsType = setOfContextsType;
		this._cacheForIndependenceTests = cacheForIndependenceTests;
		this._cacheThreshold = cacheThreshold;
		this._maxTreeWidth = maxTreeWidth;
		this._contextsFile = contextsFile;
	}

	public File getDatasetFile() {
		return new File(this.getDatasetDirectory() +
				File.separator +
				this.getDatasetName());
	}

	private String getBaseFileName() {
		String baseFileName = "";

		if (this._contextsFile.equals("")) {
			baseFileName += this.getDatasetName() +
				"_test" + this.getIndependenceTestName() +
				"_thr" + this.getIndependenceTestThreshold() +
				"_tw" + this.getMaxTreeWidth();
		} else {
			baseFileName += this.getDatasetName() + "_" +
				this.getContextsFile().getName() +
				"_test" + this.getIndependenceTestName() +
				"_thr" + this.getIndependenceTestThreshold() +
				"_tw" + this.getMaxTreeWidth();
		}

		return baseFileName;
	}


	public File getNetworkFile() {
		return new File(this.getNetworkDirectory() +
				File.separator +
				getBaseFileName() + ".net");
	}

	public File getFeatureFile() {
		return new File(this.getNetworkDirectory() +
				File.separator +
				getBaseFileName() + ".features");
	}

	public File getLogFile() {
		return new File(getLogDirectory() +
				File.separator +
				this.getBaseFileName() + ".log");
	}

	public double getIndependenceTestThreshold() {
		return this._independenceTestThreshold;
	}

	public String getIndependenceTestName() {
		return this._independenceTestName;
	}

	private String getDatasetName() {
		return this._datasetName;
	}

	private String getDatasetDirectory() {
		return this._datasetDirectory;
	}

	private String getNetworkDirectory() {
		return this._networkDirectory;
	}

	private String getLogDirectory() {
		return this._logDirectory;
	}

	public FeatureType getFeatureType() {
		return this._featureType;
	}

	public String getTypeOfContext() {
		return this._setOfContextsType;
	}

	public boolean getCacheForIndependenceTests() {
		return this._cacheForIndependenceTests;
	}

	public int getCacheThreshold() {
		return this._cacheThreshold;
	}

	public int getMaxTreeWidth() {
		return this._maxTreeWidth;
	}


	public File getContextsFile() {
		return new File(this._contextsFile);
	}
}