package cspc;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.BitSet;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import cspc.dataset.Dataset;

import cspc.feature.*;

import cspc.independenceTest.IndependenceTest;

import cspc.util.PowerSet;
import cspc.util.FileHandler;

import cspc.setting.Configuration;



public class Algorithm {
	protected final IndependenceTest _independenceTest;
	protected final Dataset _dataset;
	protected final int _numberOfVariables;
	protected boolean _flagToBreak;
	protected final Configuration _conf;
	protected final int _maxTreeWidth;
	protected long _numberOfTests;
	protected long _numberOfWeightedTests;

	public Algorithm(int numberOfVariables,
			 IndependenceTest independenceTest,
			 Dataset dataset,
			 Configuration conf) {
		this._independenceTest = independenceTest;
		this._dataset = dataset;
		this._numberOfVariables = numberOfVariables;
		this._flagToBreak = false;
		this._conf = conf;

		// If the maxTreeWidth is setted to set -1 then this means
		// that the users want to execute cspc without a threshold
		// over the size of the maximum clique (tree-width)
		this._maxTreeWidth = (this._conf.getMaxTreeWidth() == -1) ?
			this._numberOfVariables - 1 :
			this._conf.getMaxTreeWidth() + 1;

		this._numberOfTests = 0;
		this._numberOfWeightedTests = 0;
	}

	private Structure pc(Feature context, Structure structure, int maximumNumberOfVariables) {
		for (int x = 0; x < this._numberOfVariables - 1; x++) {
			BitSet adjX = structure.getAdjacencies(x);

			if (adjX.cardinality() < maximumNumberOfVariables) continue;

			// increase maximumNumberOfVariables
			this._flagToBreak = true;

			// adjancencies of x set minus y
			BitSet adjXSetMinusY = new BitSet(this._numberOfVariables);
			adjXSetMinusY.or(adjX);
			for(int y = adjX.nextSetBit(x+1); y >= 0; y = adjX.nextSetBit(y+1)) {
				adjXSetMinusY.set(y, false);

				// The elements of the power set whose size is
				// maximumNumberOfVariables
				PowerSet powerSet = new PowerSet(adjXSetMinusY,
								 maximumNumberOfVariables);

				// loop in each subset w of x's adjacencies,
				// whose size is maximumNumberOfVariables
				for (BitSet w : powerSet) {
					// System.out.println("w " + w);

					// get assignment for the variables w
					BitSet reducedContext = (BitSet)context.getVar().clone();
					reducedContext.and(w);
					List<Integer> contextualVariables = new ArrayList<Integer>();
					List<Integer> contextualValues = new ArrayList<Integer>();

					for(int i = reducedContext.nextSetBit(0);
					    i >= 0;
					    i = reducedContext.nextSetBit(i+1)) {
						contextualVariables.add(i);
						contextualValues.add(context.getVal(i));
					}

					long startTime = System.currentTimeMillis();
					this._independenceTest.independent(x,
									   y,
									   contextualVariables,
									   contextualValues);
					this._numberOfTests += 1;
					this._numberOfWeightedTests += 2 + contextualVariables.size();
					long totalTime = System.currentTimeMillis() -
startTime;

					// System.out.println(x +" " + y + "|" + reducedContext + " " + (totalTime));

					// If x and y are independent, then
					// remove edge
					if (this._independenceTest.getTruthValue()) {
						structure.rmEdge(x, y);
						break;
					}
				}

				// only add variable y to x's adjacencies if x and
				// y are dependent
				if (structure.getEdge(x, y)) adjXSetMinusY.set(y, true);
			}
		}

		return structure;
	}

	// This method constructs the initial structure which is refined for
	// each context
	private Structure constructInitialStructure() {
		Structure structure = new Structure(this._numberOfVariables);

		// define the fully structure and remove edges according to
		// unconditional independences
		for (int i = 0; i < this._numberOfVariables; i++)
			for (int j = i+1; j < this._numberOfVariables; j++) {
				structure.addEdge(i, j);

				// test unconditional independence between i
				// and j
				this._independenceTest.independent(i,
								   j,
								   new ArrayList<Integer>(),
								   new ArrayList<Integer>());

				// If the unconditional independence is true
				// then remove the edge x-y from the structure
				if (this._independenceTest.getTruthValue())
					structure.rmEdge(i, j);
			}

		return structure;
	}

	public Map<Structure, List<Integer>> execute(List<Integer> contexts) {
		Map<Structure, List<Integer>> structures =
			new HashMap<Structure, List<Integer>>();

		Structure structure;

		// Get the initial structure constructed by using only
		// unconditional independence
		Structure initialStructure = constructInitialStructure();

		Feature context = null;
		int numberOfContexts = 0;
		for (Integer idxContext : contexts) {
			long startTime = System.currentTimeMillis();

			// retrieve context from the dataset using the id
			context = this._dataset.getList().get(idxContext);

			String strOutput = "("+(++numberOfContexts)+"/"+contexts.size()+") " + context;

			structure = initialStructure.clone();

			// This loop starts from 1 because the unconditional
			// test (when maximumNumberOfVariables is 0) was
			// performed above in the method constructInitialStructure()
			for (int maximumNumberOfVariables = 1;
			     maximumNumberOfVariables <  this._maxTreeWidth;
			     maximumNumberOfVariables++) {
				this._flagToBreak = false;
				// System.out.println("k: " + maximumNumberOfVariables);

				structure = pc(context, structure, maximumNumberOfVariables);

				if (!this._flagToBreak) break;
			}

			// Does this new structure exist? If it doesn't then create an entry
			List<Integer> contextsOfStructure =
				structures.containsKey(structure) ?
				structures.get(structure) : new ArrayList<Integer>();

			contextsOfStructure.add(idxContext);
			structures.put(structure, contextsOfStructure);

			// Write the log file with the total time expended by
			// cspc in the current context
			long totalTime = System.currentTimeMillis() - startTime;
			FileHandler.writeMessage(_conf.getLogFile(),
						 strOutput + " in " + totalTime + " ms.");
		}


		return structures;
	}

	public long getNumberOfTests() {
		return this._numberOfTests;
	}

	public long getNumberOfWeightedTests() {
		return this._numberOfWeightedTests;
	}
}
