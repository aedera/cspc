package cspc.feature;

import java.util.Arrays;
import java.util.BitSet;

/**
 * This class represents a mixed feature (i.e., a conjunction over both true and
 * false variables).
 */

public class MixedFeature extends Feature {
	private final BitSet[] _val;

	public MixedFeature(int n) {
		super(n);
		this._val = new BitSet[n];
	}

	public MixedFeature(MixedFeature f) {
		super(f.getNumberOfDomainVariables(), f.getVar());

		this._val = new BitSet[this.getNumberOfDomainVariables()];

		for(int i = this.getVar().nextSetBit(0); i >= 0; i = this.getVar().nextSetBit(i+1))
			this.addVal(i, f.getVal(i));

		// this._val = Arrays.<BitSet>copyOf(f._val, f.getVar().length());
	}

        @Override
	public boolean isSatisfied(Feature otherFeature) {
		for(int i = this.getVar().nextSetBit(0); i >= 0; i = this.getVar().nextSetBit(i+1)) {
			if(!otherFeature.getVar().get(i)) return false;

			if (this.getVal(i) != otherFeature.getVal(i)) return false;
		}

		return true;
	}

	public void addVal(int var, Integer val) {
		if (val == null) {
			this.rmVar(var);
			_val[var] = null;
			return;
		}

		this.addVar(var);

		if (this._val[var] == null) {
			this._val[var] = new BitSet();//this.getVar().length());
		} else {
			this._val[var].clear();
		}

		this._val[var].set(val);
	}

        @Override
	public Integer getVal(int var) {
		if (this._val[var] != null)
			return this._val[var].nextSetBit(0);

		return null;
	}

        @Override
	public int getLength() {
		int count = 0;

		for (int i = 0; i < _val.length; i++)
			if (_val[i] != null) count++;

		return count;
	}

        @Override
	public MixedFeature clone() {
		return new MixedFeature(this);
	}

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 67 * hash + ( this._val == null ? 0 : Arrays.deepHashCode(this._val));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final MixedFeature other = (MixedFeature) obj;
        return Arrays.deepEquals(this._val, other._val);
    }



        @Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();


		for(int i=this.getVar().nextSetBit(0); i>=0; i=this.getVar().nextSetBit(i+1)) {
			stringBuilder.append("+v" + i + "_" + getVal(i) + " ");
		}

		return stringBuilder.toString().trim();
	}

	public static void main(String[] args) {

		Feature f = new MixedFeature(3);
		f.addVal(0, 1);
		// f.addVal(1, 2);
		f.addVal(2, 0);

		System.out.println(f);
		System.out.println(f.getLength());
		System.out.println(f.hashCode());

		f.addVal(1, 2);

		System.out.println(f);
		System.out.println(f.getLength());
		System.out.println(f.hashCode());

		f.addVal(1, null);

		System.out.println(f);
		System.out.println(f.getLength());
		System.out.println(f.hashCode());
	}
}