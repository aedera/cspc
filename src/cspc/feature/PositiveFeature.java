package cspc.feature;

import java.util.BitSet;


/**
 * A positive feature is represented as an array with the true
 * variables.
 *
 **/
public class PositiveFeature extends Feature {

	public PositiveFeature(int n) {
		super(n);
	}

	protected PositiveFeature(int n, BitSet var) {
		super(n, var);
	}

	public boolean isSatisfied(Feature otherFeature) {
		for(int i = this.getVar().nextSetBit(0); i >= 0; i = this.getVar().nextSetBit(i+1))
			if(!otherFeature.getVar().get(i)) return false;

		return true;
	}


	public void addVal(int var, Integer val) {
		if (val == null || val == 0) {
			this.rmVar(var);
			return;
		}

		this.addVar(var);
	}

	public Integer getVal(int var) {
		return this.getVar().get(var) ? 1 : 0;
	}


	public int getLength() {
		return this.getVar().cardinality();
	}

	public PositiveFeature clone() {
		return new PositiveFeature(this.getNumberOfDomainVariables(),
					   (BitSet)this.getVar().clone());
	}


	public boolean equals(Object object) {
		if (object instanceof PositiveFeature) {
			PositiveFeature f = (PositiveFeature) object;

			return this.getVar().equals(f.getVar());
		} else
			return false;
	}

	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		for(int i = this.getVar().nextSetBit(0); i >= 0; i = this.getVar().nextSetBit(i+1))
			stringBuilder.append("+v" + i + "_1 ");

		return stringBuilder.toString().trim();
	}
}