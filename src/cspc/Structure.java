package cspc;

import java.util.BitSet;
import java.util.Arrays;


public class Structure {
	private final BitSet[] _adjacencies;

	public Structure(int numberOfVariables) {
		this._adjacencies = new BitSet[numberOfVariables];

		for (int i = 0; i < numberOfVariables; i++)
			this._adjacencies[i] = new BitSet(numberOfVariables);
	}

	public Structure(Structure structure) {
		this._adjacencies = new BitSet[structure.getNumberOfVariables()];

		for (int i = 0; i < structure.getNumberOfVariables(); i++)
			this._adjacencies[i] = (BitSet) structure.getAdjacencies(i).clone();
	}

	public BitSet getAdjacencies(int variable) {
		return this._adjacencies[variable];
	}

	public boolean getEdge(int x, int y) {
		return this._adjacencies[x].get(y);
	}

	public void addEdge(int x, int y) {
		if (this._adjacencies[x] == null)
			this._adjacencies[x] = new BitSet(this.getNumberOfVariables());

		if (this._adjacencies[y] == null)
			this._adjacencies[y] = new BitSet(this.getNumberOfVariables());

		this._adjacencies[x].set(y, true);
		this._adjacencies[y].set(x, true);
	}

	public void rmEdge(int x, int y) {
		this._adjacencies[x].set(y, false);
		this._adjacencies[y].set(x, false);
	}

	public int getNumberOfVariables() {
		return _adjacencies.length;
	}

	public Structure clone() {
		return new Structure(this);
	}

	public boolean equals(Object object) {
		if (object instanceof Structure) {
			Structure st = (Structure) object;

			return Arrays.deepEquals(this._adjacencies, st._adjacencies);
		}
		else
			return false;
	}

	public int hashCode() {
		return Arrays.deepHashCode(this._adjacencies.clone());
	}

	// the space complexity of the output string is \choose{n}{2} in the
	// worst case (fully connected graph)
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		int j;
		// only print the adjacencies of n - 1 variables, the last one
		// is not necessary (they can be inferred from the remaining
		// variables)
		for (int i = 0; i < this.getNumberOfVariables() - 1; i++) {
			for (j = i + 1; j < this.getNumberOfVariables(); j++)
				if (this._adjacencies[i].get(j))
					stringBuilder.append(j + " ");

			if (this._adjacencies[i].get(j))
				stringBuilder.append(j);

			stringBuilder.append(",");
		}

		return stringBuilder.toString().replaceAll("\\s,", ",").replaceAll(",$","");
	}
}
