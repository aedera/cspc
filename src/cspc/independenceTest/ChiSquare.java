package cspc.independenceTest;

import java.util.HashMap;
import java.util.List;

import cspc.dataset.Dataset;

import cspc.setting.FeatureType;

public class ChiSquare extends IndependenceTest {
	private static double gamser, gammcf, gln;

	private static double FPMIN = 1.0e-30;

	private static int ITMAX = 2000;

	private static double EPS = 3.0e-7;

	private double df;


	public ChiSquare(Dataset data, double threshold) {
		super(data, threshold);
	}

	public ChiSquare(Dataset data, double threshold, FeatureType featureType) {
		super(data, threshold, featureType);
	}

	public double
	computeLogPValue(int x, int y, List<Integer> z, List<Integer> context) {
		HashMap<Integer, int[][]> contTables  =
			computeContingencyTables(x, y, z, context);

		long numValX = getDataset().getCard(x);
		long numValY = getDataset().getCard(y);
		int numSlices = contTables.size();
		df = (numValX * numValY - 1) * numSlices;

		/** Computes conditional chsq value */
		double chsq = 0.0;

		for(int[][] ct : contTables.values()) chsq += chiVal(ct);

		double logPValue = chsq == 0 ? 0.0 : ln_gammq(.5 * df, 0.5 * chsq);

		return logPValue;
	}

	protected double chiVal(int[][] matrix) {
		int nrows, ncols, row, col;
		double[] rtotal, ctotal;
		double expect = 0, chival = 0, n = 0;

		nrows = matrix.length;
		ncols = matrix[0].length;
		rtotal = new double[nrows];
		ctotal = new double[ncols];
		for (row = 0; row < nrows; row++) {
			for (col = 0; col < ncols; col++) {
				rtotal[row] += matrix[row][col];
				ctotal[col] += matrix[row][col];
				n += matrix[row][col];
			}
		}

		chival = 0.0;
		for (row = 0; row < nrows; row++) {
			for (col = 0; col < ncols; col++) {
				if (ctotal[col] == 0 || rtotal[row] == 0) {
					df--;
					continue;
				}
				expect = (ctotal[col] * rtotal[row]) / n;

				if (expect != 0)
					chival += computeCell(matrix[row][col], expect);
			}
		}

		return chival;
	}

	//Oij is the frequency count and Eij is the null-hypthesis (independence) model expected value
	protected double computeCell(double Oij, double Eij){
		return Math.abs(Oij - Eij) * Math.abs(Oij - Eij) / Eij;
	}


	public static double gammq(double df, double x) {
		if (x < 0.0 || df <= 0.0) {
			System.err.println("Invalid arguments in routine gammq()");
			(new Exception()).printStackTrace();
			System.exit(0);
		}

		if (x < (df + 1.0)) {
			if (x == 0)
				return 1;

			gser(df, x);
			return 1.0 - gamser;
		} else {
			gcf(df, x);
			return gammcf;
		}
	}

	static void gser(double df, double x) {
		int n;
		double sum, del, ap;

		gln = gammln(df);
		if (x < 0.0) {
			System.err.println("x less than 0 in routine gser()");
			return;
		} else {
			ap = df;
			del = sum = 1.0 / df;
			for (n = 1; n <= ITMAX; n++) {
				++ap;
				del *= x / ap;
				sum += del;
				if (Math.abs(del) < Math.abs(sum) * EPS) {
					gamser = sum * Math.exp(-x + df * Math.log(x) - (gln));
					return;
				}
			}
			System.err.println("a too large, ITMAX too small in routine gser()");
			return;
		}
	}

	static void gcf(double a, double x) {
		int i;
		double an, b, c, d, del, h;

		gln = gammln(a);
		b = x + 1.0 - a;
		c = 1.0 / FPMIN;
		d = 1.0 / b;
		h = d;
		for (i = 1; i <= ITMAX; i++) {
			an = -i * (i - a);
			b += 2.0;
			d = an * d + b;
			if (Math.abs(d) < FPMIN)
				d = FPMIN;
			c = b + an / c;
			if (Math.abs(c) < FPMIN)
				c = FPMIN;
			d = 1.0 / d;
			del = d * c;
			h *= del;
			if (Math.abs(del - 1.0) < EPS)
				break;
		}
		if (i > ITMAX)
			System.err.println("a too large, ITMAX too small in gcf()");
		gammcf = Math.exp(-x + a * Math.log(x) - (gln)) * h;
	}

	static double gammln(double xx) {
		double x, y, tmp, ser;
		double cof[] = { 76.18009172947146, -86.50532032941677,
				24.01409824083091, -1.231739572450155, 0.1208650973866179e-2,
				-0.5395239384953e-5 };
		int j;

		y = x = xx;
		tmp = x + 5.5;
		tmp -= (x + 0.5) * Math.log(tmp);
		ser = 1.000000000190015;
		for (j = 0; j <= 5; j++)
			ser += cof[j] / ++y;
		return -tmp + Math.log(2.5066282746310005 * ser / x);
	}

	public static double ln_gammq(double df, double x) {

		if (x < 0.0 || df <= 0.0) {
			System.err.println("Invalid arguments in routine gammq()");
		}
		if (x < (df + 1.0)) {
			if (x == 0) return 0;

			return Math.log(-Math.expm1(ln_gser(df, x)));
		} else {
			return ln_gcf(df, x);
		}
	}

	static double ln_gser(double df, double x) {
		int n;
		double sum, del, ap;

		gln = gammln(df);
		if (x < 0.0) {
			System.err.println("x less than 0 in routine gser()");
			return -1;
		} else {
			ap = df;
			del = sum = 1.0 / df;
			for (n = 1; n <= ITMAX; n++) {
				++ap;
				del *= x / ap;
				sum += del;
				if (Math.abs(del) < Math.abs(sum) * EPS) {
					// gamser = sum * Math.exp(-x + df * Math.log(x) - (gln));
					return Math.log(sum) - x + df * Math.log(x) - (gln);
				}
			}
			System.err.println("a too large, ITMAX too small in routine gser()");
			return -1;
		}
	}

	static double ln_gcf(double a, double x) {
		int i;
		double an, b, c, d, del, h;

		gln = gammln(a);
		b = x + 1.0 - a;
		c = 1.0 / FPMIN;
		d = 1.0 / b;
		h = d;

		for (i = 1; i <= ITMAX; i++) {
			an = -i * (i - a);
			b += 2.0;
			d = an * d + b;
			if (Math.abs(d) < FPMIN)
				d = FPMIN;
			c = b + an / c;
			if (Math.abs(c) < FPMIN)
				c = FPMIN;
			d = 1.0 / d;
			del = d * c;
			h *= del;
			if (Math.abs(del - 1.0) < EPS)
				break;
		}

		if (i > ITMAX)
			System.err.println("a too large, ITMAX too small in gcf()");

		return -x + a * Math.log(x) - (gln) + Math.log(h);
	}
}
