package cspc.independenceTest;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.BitSet;

import cspc.dataset.Dataset;
import cspc.feature.*;
import cspc.adtree.ADNode;
import cspc.setting.FeatureType;
import cspc.util.Util;


abstract public class IndependenceTest {
	private Dataset _dataset;
	private double _threshold;
	private boolean _truthValue;
	private double _logPValue;
	private ADNode _adTree;
	private int _x;
	private int _y;
	private List<Integer> _z;
	private List<Integer> _context;
	private FeatureType _featureType;

	public IndependenceTest(Dataset data, double threshold){
		this._dataset = data;
		this._threshold = threshold;
		this._truthValue = false;
		this._logPValue = 0;
		this._adTree = null;
		_x = -1;
		_y = -1;
		_z = null;
		_context = null;
		_featureType = FeatureType.MIXED;
	}

	public IndependenceTest(Dataset data, double threshold, FeatureType featureType){
		this(data, threshold);
		_featureType = featureType;
	}

	public abstract double computeLogPValue(int x, int y, List<Integer> z, List<Integer> context);

	public boolean independent(int x, int y, List<Integer> z, List<Integer> context) {
		_x = x;
		_y = y;
		_z = new ArrayList<Integer>(z);
		_context = new ArrayList<Integer>(context);

		this._logPValue = computeLogPValue(x, y, z, context);

		this._truthValue = this._logPValue > Math.log(this._threshold);

		return (this._truthValue);
	}

	public boolean independent(int x, int y, List<Integer> z) {
		return independent(x, y, z, null);
	}

	private int[] computeContingencyTableFromADTree(BitSet vars, Map<Integer,Integer> context) {
		int [] cards = new int[this._dataset.getNumberOfVariables()];
		for (int i = 0; i < cards.length; i++)
			cards[i] = this._dataset.getCard(i);

		return _adTree.makeContab(vars, context, cards, this._dataset);
	}

	// This method computes the contingency tables for a
	// triplet. If the parameter context is different to null, then
	public HashMap<Integer, int[][]>
		computeContingencyTables(int x, int y, List<Integer> z, List<Integer> context) {

		int[] counts;

		BitSet vars = new BitSet(this._dataset.getNumberOfVariables());
		vars.set(x, true);
		vars.set(y, true);

		Map<Integer, Integer> map = new HashMap<Integer,Integer>();

		for (int i = 0; i < z.size(); i++) {
			vars.set(z.get(i), true);
			map.put(z.get(i), context.get(i));
		}

		counts = (_adTree != null) ?
			computeContingencyTableFromADTree(vars, map) :
		        this._dataset.computeSufficientStatistics(vars, map);

		HashMap<Integer, int[][]> contTables = new HashMap<Integer, int[][]>();

		int[][] contTable = new int[this._dataset.getCard(x)][this._dataset.getCard(x)];

		contTable[0][0] = counts[0];
		contTable[0][1] = counts[1];
		contTable[1][0] = counts[2];
		contTable[1][1] = counts[3];

		// active garbage collector to drop counts
		counts = null;

		contTables.put(0, contTable);

		return contTables;
	}

	public void setADTree(ADNode adtree) { this._adTree = adtree; }

	public void setThreshold(double threshold) { this._threshold = threshold; }

	public double getThreshold() { return this._threshold; }

	public boolean getTruthValue() { return this._truthValue; }

	public double getPValue() { return Math.exp(this._logPValue); }

	public Dataset getDataset() { return this._dataset; }

	public String toString() {
		String output = getTruthValue()? "I" : "!I";
		output += " " + _x + ", " + _y + " | (";

		for (int i = 0; i < _z.size(); i++) {
			output += _z.get(i) + "=";

			if (_context != null) output += _context.get(i);

			output += " ";
		}

		output = output.trim() + ") p-value: " + getPValue();

		return output;
	}
}
