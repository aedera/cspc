package cspc.util;

import java.util.BitSet;
import java.util.Stack;
import java.util.Iterator;

public class PowerSet implements Iterator<BitSet>, Iterable<BitSet> {
	private int _minSize;
	private int _numberOfBits;
	private final BitSet _set;
	private BitSet _r;
	private Stack<Node> _stack;


	public PowerSet(BitSet set, int minSize) {
		this._set = set;
		this._numberOfBits = set.cardinality();//numberOfBits;
		this._minSize = minSize;

		this._r = new BitSet(this._numberOfBits);

		this._stack = new Stack<Node>();

		Node root = new Node();
		root._r = new BitSet(this._numberOfBits);
		this._stack.push(root);
	}

	private void nextElement() {
		int numberOfFree;
		int numberOfUnassignedElement;
		boolean newBranch;
		Node previousNode;

		while (!this._stack.empty()) {
			Node node = this._stack.pop();

			node._r.set(node._branch, true);

			numberOfFree = _numberOfBits - node._branch + 1;
			numberOfUnassignedElement = _minSize - node._level + 1;
			newBranch = numberOfFree - numberOfUnassignedElement > 0;

			if (newBranch) {
				Node childNode = new Node(node._level,
							  node._branch + 1);

				childNode._r = (BitSet) node._r.clone();
				childNode._r.set(node._branch, false);
				childNode._r.set(childNode._branch, true);
				this._stack.push(childNode);
			}

			// if this is not a leaf, then create a new child node
			if (node._level+1 == this._minSize) {
				this._r = (BitSet) node._r.clone();

				break;
			} else  {
				Node childNode = new Node(node._level + 1,
							  node._branch + 1);

				childNode._r = (BitSet)node._r.clone();
				this._stack.push(childNode);
			}
		};
	}

	public boolean hasNext() {
		return !this._stack.empty();
	}

	public BitSet next() {
		if (this._minSize == 0) this._stack.pop();
		else nextElement();

		BitSet returnSet = new BitSet(_r.length());

		int ii = 0;
		for(int i = _set.nextSetBit(0); i >= 0; i = _set.nextSetBit(i+1))
			if (_r.get(ii++))
				returnSet.set(i);


		return returnSet;
	}

	public void remove() {
		throw new UnsupportedOperationException("Not Supported!");
	}

	public Iterator<BitSet> iterator() {
		return this;
	}

	public static void main(String[] args) {
		int n = 10;

		BitSet b = new BitSet(n);

		b.set(0, true);
		b.set(1, true);
		b.set(2, true);
		b.set(3, true);
		b.set(4, true);
		b.set(5, true);

		PowerSet pset = new PowerSet(b, 6);

		System.out.println("Let go baby!");
		for (BitSet s : pset) System.out.println("== " + s);
	}

	class Node {
		public int _level;
		public int _branch;
		public BitSet _r;

		public Node() {
			_level = 0;
			_branch = 0;
		}

		public Node(int level, int branch) {
			_level = level;
			_branch = branch;
		}

		public String toString() {
			String output = System.identityHashCode(this) +
				" l:" + _level +
				" b:" + _branch;

			return output;
		}
	}
}
