package cspc.util;

import java.util.Set;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Collection;

import cspc.feature.*;
import cspc.Structure;



public class Util {
	// most significantbit determines how to read the bitset. For
        // instance, suppose three binary variables x_0, x_1, and x_2, then
        // the assignment x_0=0, x_1=0, x_2=1 with mostSignificantBit equals
        // false is 4 in decimal. However if mostSignificantBit equals true
        // then in decimal is 1.
	public static Feature
	indexToAssignment(int index,
			  List<Integer> var,
			  List<Integer> card,
			  int numberOfVariables,
			  boolean mostSignificantBit) {
		Feature feature = new BinaryFeature(numberOfVariables);

		double tmp = index;

		if (mostSignificantBit)
			for (int i = var.size() - 1; i > -1; i--) {
				feature.addVal(var.get(i),
					       (int)tmp % card.get(var.get(i)));

				tmp /= card.get(var.get(i));
			}
		else
			for (int i = 0; i < var.size(); i++) {
				feature.addVal(var.get(i),
					       (int)tmp % card.get(var.get(i)));

				tmp /= card.get(var.get(i));
			}


		return feature;
	}

	public static Feature
	indexToAssignment(int index,
			  List<Integer> var,
			  List<Integer> card,
			  int numberOfVariables) {
		return indexToAssignment(index, var, card, numberOfVariables, false);
	}

	public static int
	assignmentToIndex(Feature assignment, List<Integer> card, boolean mostSignificantBit) {
		int index = 0;

		for(int i = assignment.getVar().nextSetBit(0);
		    i >= 0;
		    i = assignment.getVar().nextSetBit(i+1)) {
			Integer val = assignment.getVal(i);

			if (val != 0)
				index += (mostSignificantBit) ?
					Math.pow(card.get(i), card.size() - i - 1) :
					Math.pow(card.get(i), i);
		}

		return index;
	}

	public static int
	assignmentToIndex(Feature assignment, List<Integer> card) {
		return assignmentToIndex(assignment, card, false);
	}

	public static void main(String[] aa) {
		java.util.List<Integer> var = new java.util.ArrayList<Integer>();
		java.util.List<Integer> card = new java.util.ArrayList<Integer>();

		var.add(0);
		var.add(1);
		var.add(2);
		var.add(3);

		card.add(2);
		card.add(2);
		card.add(2);
		card.add(2);

		int numberOfVariables = card.size();

		for (int i = 0; i < 16; i++) {
			Feature f1 = Util.indexToAssignment(i,
							    var,
							    card,
							    numberOfVariables,
							    false);

			System.out.println(i + " " + f1  + " " +
					   assignmentToIndex(f1, card, false)+ " " +
					   assignmentToIndex(f1, card, true));

			Feature f2 = Util.indexToAssignment(i,
							    var,
							    card,
							    numberOfVariables,
							    true);

			// System.out.println(i + " " + f2 + " " +
			// 		   assignmentToIndex(f2, card, false) + " " +
			// 		   assignmentToIndex(f2, card, true));
			System.out.println("---");
		}
	}

	// /**
	//  * Finding maximal clique in graph by using Bron-Kerbosh Algorithm version 1.
	//  *
	//  * @param clique     Set whose elements are maximal cliques in the graph
	//  * @param compsub    Potential clique between calls
	//  * @param candidates Candidates to be part of a clique
	//  * @param not        Nodes not used between calls
	//  *
	//  * @todo improve selection to pivot by selectioning a node with many
	//  * neighborshood. The improve in the algorithm become it to version 2.
	//  */
	public static void
	bronKerboschAlgorithm(Collection<BitSet> cliques,
			      BitSet compsub,
			      BitSet candidates,
			      BitSet not,
			      Structure neighbors) {
		// Is found a clique?
		if (candidates.cardinality() == 0 && not.cardinality() == 0) {
			cliques.add(compsub);
			return;
		}

		BitSet nodesToSelect = (BitSet) candidates.clone();
		nodesToSelect.or(not);
		int pivot = nodesToSelect.nextSetBit(0);
		BitSet neighborsOfPivot = neighbors.getAdjacencies(pivot);
		BitSet inspect = (BitSet) candidates.clone();

		for (int i = neighborsOfPivot.nextSetBit(0); i >= 0; i = neighborsOfPivot.nextSetBit(i+1))
			inspect.set(i, false); //removeAll(neighborsOfPivot);

		BitSet newCompsub;
		BitSet neighborsOfSelected;
		BitSet newCandidates;
		BitSet newNot;
		for (int selectedNode = inspect.nextSetBit(0);
		     selectedNode >= 0;
		     selectedNode = inspect.nextSetBit(selectedNode+1)) {

			newCompsub = (BitSet) compsub.clone();

			newCompsub.set(selectedNode, true);

			neighborsOfSelected = neighbors.getAdjacencies(selectedNode);

			newCandidates = (BitSet) candidates.clone();

			// newCandidates.retainAll(neighborsOfSelected);
			newCandidates.and(neighborsOfSelected);

			newNot = (BitSet) not.clone();

			// newNot.retainAll(neighborsOfSelected);
			newNot.and(neighborsOfSelected);

			bronKerboschAlgorithm(cliques,
					      newCompsub,
					      newCandidates,
					      newNot,
					      neighbors);

			candidates.set(selectedNode, false);
			not.set(selectedNode, true);
		}

	}

	public static Collection<BitSet> factorizeToCompleteSubgraphs(Structure neighbors)
	{
		Collection<BitSet> cliques = new java.util.HashSet<BitSet>();

		BitSet initialCandidates = new BitSet(neighbors.getNumberOfVariables());
		initialCandidates.set(0, neighbors.getNumberOfVariables(), true);
		bronKerboschAlgorithm(cliques,
				      new BitSet(neighbors.getNumberOfVariables()),
				      initialCandidates,
				      new BitSet(neighbors.getNumberOfVariables()),
				      neighbors);

		return cliques;
	}
}