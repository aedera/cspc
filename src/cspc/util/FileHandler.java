package cspc.util;

import cspc.adtree.ADNode;
import cspc.feature.*;
import cspc.dataset.Dataset;
import cspc.dataset.DatasetType;
import cspc.setting.FeatureType;
import cspc.Structure;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.List;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FileHandler {
	public static void writeADTree(File outputFile, ADNode adtree) {
		try {
			FileOutputStream fos = new FileOutputStream(outputFile);
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(adtree);
			oos.flush();
			oos.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static ADNode readADTree(File inputFile) {
		ADNode adtree = null;

		try {
			FileInputStream fis = new FileInputStream(inputFile);

			ObjectInputStream ois = new ObjectInputStream(fis);

			adtree = (ADNode) ois.readObject();
			ois.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return adtree;
	}

private static void
	readWDataDataset(File inputFile, Dataset dataset) throws IOException {
		// Open the file reader
		FileReader fileReader = new FileReader(inputFile);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		int numberOfVariables = 0;
		String line = null;

		while ((line = bufferedReader.readLine()) != null) {
			String[] parts = line.trim().split("\\|");
			String[] items = parts[1].split(",");

			numberOfVariables = items.length;
			Feature example = new BinaryFeature(numberOfVariables);

			for (int i = 0; i < numberOfVariables; i++) {
				example.addVal(i, Integer.parseInt(items[i]));
			}

			for (int i = 0; i < Integer.parseInt(parts[0]); i++) {
				dataset.addExample(example);
			}
		}

		bufferedReader.close();

		dataset.setNumberOfVariables(numberOfVariables);
	}

	public static Dataset
	readDataset(File inputFile) {
		Dataset dataset = new Dataset();

		try {
			readWDataDataset(inputFile, dataset);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		return dataset;
	}

	public static void
	createFeatureFile(File featureFile) {
		try {
			FileWriter fileWriter = new FileWriter(featureFile);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			bufferedWriter.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void
	writeFeature(File featureFile, Collection<Feature> features) {
		try {
			// Append feature of this file
			FileWriter fileWriter = new FileWriter(featureFile, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			for (Feature feature : features)
				if (feature.getLength() != 0)
					bufferedWriter.write("0.000 " + feature.toString() + "\n");

			// Close the writer
			bufferedWriter.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void writeMessage(File logFile, String message) {
		try {
			// Open the writer
			FileWriter fileWriter = new FileWriter(logFile, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// Write the message
			bufferedWriter.write(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").format(new Date()) + "\t");
			bufferedWriter.write(message + "\n");

			// Close the writer
			bufferedWriter.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void
	writeNetwork(Map<Structure, List<Integer>> structure,  File networkFile) {
		try {
			FileWriter fileWriter;

			// Clean networkFile
			if (structure == null) {
				fileWriter = new FileWriter(networkFile);
				return;
			}

			fileWriter = new FileWriter(networkFile, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			int i = 0;
			for (Map.Entry<Structure, List<Integer>> s : structure.entrySet()) {
				// Write the network
				bufferedWriter.write(s.getKey().toString());
				bufferedWriter.write(";");

				for (i = 0; i < s.getValue().size() - 1; i++)
					bufferedWriter.write(s.getValue().get(i) + " ");

				bufferedWriter.write(s.getValue().get(i)+ "\n");
			}

			// Close the writer
			bufferedWriter.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static List<Integer> readContexts(File inputFile)
		throws IOException {
		// Open the file reader
		FileReader fileReader = new FileReader(inputFile);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		List<Integer> contexts = new java.util.ArrayList<Integer>();

		int numberOfVariables = -1;
		String line = null;
		while ((line = bufferedReader.readLine()) != null)
			contexts.add(Integer.parseInt(line.trim()));

		bufferedReader.close();

		return contexts;
	}

	public static Map<Structure,List<Feature>>
		readNetworks(String strInputFile, int numberOfVariables)
		throws IOException {
		// Open the file reader
		File inputFile = new File(strInputFile);
		FileReader fileReader = new FileReader(inputFile);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		Map<Structure,List<Feature>> structures = new java.util.HashMap();

		String line = null;

		String[] items;
		Structure structure;
		int k =0;
		while ((line = bufferedReader.readLine()) != null) {
			// System.out.println(line);
			// System.exit(1);

			items = line.trim().split(" ");

			structure = new Structure(numberOfVariables);
			for (int j = 1; j < items.length; j++) {
				if (Integer.parseInt(items[j]) == 1)
					structure.addEdge(0, j);
			}

			// Read the structure
			for (int i = 1; i < numberOfVariables; i++) {
				line = bufferedReader.readLine();
				items = line.trim().split(" ");

				for (int j = i+1; j < items.length; j++)
					if (Integer.parseInt(items[j]) == 1)
						structure.addEdge(i, j);
			}

			k++;
			Feature context = new BinaryFeature(numberOfVariables);
			// Read the context
			line = bufferedReader.readLine();
			items = line.trim().split(" ");

			for (int i = 0; i < numberOfVariables; i++)
				context.addVal(i, Integer.parseInt(items[i].split("_")[1]));


			// System.out.println(line);
			// System.out.println(context);
			// System.out.println("--");

			// if (k > 20) {


			// System.exit(1);

			// }



			List<Feature> contextsOfStructure;

			// Does this new structure exist? If it doesn't then create an entry
			contextsOfStructure = structures.containsKey(structure) ?
				structures.get(structure) : new java.util.ArrayList<Feature>();

			contextsOfStructure.add(context);
			structures.put(structure, contextsOfStructure);
		}

		bufferedReader.close();

		return structures;
	}
}